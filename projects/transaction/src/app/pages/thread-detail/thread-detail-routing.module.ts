import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule, Routes } from '@angular/router';
import { ThreadDetailCreateComponent } from './thread-detail-create/thread-detail-create.component';
import { ThreadDetailListComponent } from './thread-detail-list/thread-detail-list.component';

const routes: Routes = [
  {
    path: ":id",
    component: ThreadDetailCreateComponent,
  },
  {
    path: "thread/dtl/list",
    component: ThreadDetailListComponent,
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ThreadDetailRoutingModule { }
