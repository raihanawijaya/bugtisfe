import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThreadDetailListComponent } from './thread-detail-list/thread-detail-list.component';
import { ThreadDetailCreateComponent } from './thread-detail-create/thread-detail-create.component';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ThreadDetailRoutingModule } from './thread-detail-routing.module';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [
    ThreadDetailListComponent,
    ThreadDetailCreateComponent
  ],
  imports: [
    CommonModule,
    ThreadDetailRoutingModule,
    FormsModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    RadioButtonModule,
    ConfirmDialogModule,
    ConfirmPopupModule
  ],
  exports: [
    ThreadDetailListComponent,
    ThreadDetailCreateComponent
  ]
})
export class ThreadDetailModule { }
