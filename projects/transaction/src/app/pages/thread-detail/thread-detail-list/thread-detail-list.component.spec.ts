import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadDetailListComponent } from './thread-detail-list.component';

describe('ThreadDetailListComponent', () => {
  let component: ThreadDetailListComponent;
  let fixture: ComponentFixture<ThreadDetailListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThreadDetailListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
