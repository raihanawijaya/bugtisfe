import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadDetailCreateComponent } from './thread-detail-create.component';

describe('ThreadDetailCreateComponent', () => {
  let component: ThreadDetailCreateComponent;
  let fixture: ComponentFixture<ThreadDetailCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThreadDetailCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadDetailCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
