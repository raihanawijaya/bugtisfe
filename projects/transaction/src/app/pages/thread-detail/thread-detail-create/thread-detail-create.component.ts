import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { BASE_URL_DOWNLOAD_ATTACHMENT } from 'projects/master/src/app/constant/general-constant';
import { AuthService } from 'projects/ticketing/src/app/services/auth.service';
import { FindAllThreadDtlByHdrResData } from '../../../models/thread-detail/find-all-thread-dtl-by-hdr-res-data';
import { FindAllThreadDtlResData } from '../../../models/thread-detail/find-all-thread-dtl-res-data';
import { FindByIdThreadDtlResData } from '../../../models/thread-detail/find-by-id-thread-dtl-res-data';
import { InsertThreadDtlReq } from '../../../models/thread-detail/insert-thread-dtl-req';
import { FindByIdThreadHdrResData } from '../../../models/thread-header/find-by-id-thread-hdr-res-data';
import { UpdateThreadHdrReq } from '../../../models/thread-header/update-thread-hdr-req';
import { ThreadDetailService } from '../../../services/thread-detail.service';
import { ThreadHeaderService } from '../../../services/thread-header.service';
import { ConfirmationService, ConfirmEventType, MessageService } from 'primeng/api';

@Component({
  selector: 'app-thread-detail-create',
  templateUrl: './thread-detail-create.component.html',
  styleUrls: ['./thread-detail-create.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class ThreadDetailCreateComponent implements OnInit {

  file: any;
  url = BASE_URL_DOWNLOAD_ATTACHMENT
  status: string = "OPEN"
  fileName = ""
  data: FindAllThreadDtlResData[] = []
  dataListDtlByHdr: FindAllThreadDtlByHdrResData[] = []
  dataHdr: FindByIdThreadHdrResData = new FindByIdThreadHdrResData()
  dataDtl: FindByIdThreadDtlResData = new FindByIdThreadDtlResData()
  threadDtlReq = new InsertThreadDtlReq()
  updateThreadStatus = new UpdateThreadHdrReq()

  isAuthorize?: boolean
  statusButton?: boolean
  isClose?: boolean;

  constructor(private threadHdrService: ThreadHeaderService, private threadDtlService: ThreadDetailService,
    private activatedRoute: ActivatedRoute, private title: Title, private route: Router,
    private authService: AuthService, private messageService: MessageService, private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.title.setTitle("Thread")
    this.threadHdr()

    this.activatedRoute.params.subscribe(res => {
      this.threadDtlService.findByHdr(res.id).subscribe(res => {
        this.dataListDtlByHdr = res.data!
        console.log(this.dataListDtlByHdr);
      })
    })

    if (this.authService.getProfile().roleCode == 'ACC') {
      this.isAuthorize = true
      this.statusButton = true
    } else if (this.authService.getProfile().roleCode == 'AGN') {
      this.isAuthorize = true
      this.statusButton = false
    } else if (this.authService.getProfile().roleCode == 'ADM' || this.authService.getProfile().roleCode == 'CST') {
      this.isAuthorize = false
      this.statusButton = false
    }
  }

  fileChange(data: any): void {
    if (data.files && data.files[0]) {
      this.file = data.files[0]
      this.fileName = data.files[0].name
    }
  }

  onClick(): void {
    this.threadDtlService.insert(this.threadDtlReq, this.file).subscribe(res => {
      this.route.navigate(['/tickets'])
    })
  }

  threadHdr(id?: number): void {
    this.activatedRoute.params.subscribe(res => {
      this.threadHdrService.findById(res.id).subscribe(res => {
        this.dataHdr = res.data!
        if (this.dataHdr.status == "OPEN") {
          this.status = "CLOSE"
          this.isClose = true;
        }
        if (this.dataHdr.status == "CLOSE") {
          this.status = "REOPEN"
        }
        if (this.dataHdr.status == "REOPEN") {
          this.status = "CLOSE"
          this.isClose = true;
        }
        this.threadDtlReq.threadHdr = this.dataHdr.id
      })
    })
  }

  statusChange(event: Event): void {
    this.confirmationService.confirm({
      target: event.target!,
      message: 'Do you want to change this status?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        if (this.dataHdr.status == "OPEN") {
          this.activatedRoute.params.subscribe(res => {
            this.threadHdrService.findById(res.id).subscribe(res => {
              this.updateThreadStatus.id = res.data?.id
              this.updateThreadStatus.status = "CLOSE"
              this.updateThreadStatus.version = res.data?.version
              this.threadHdrService.update(this.updateThreadStatus).subscribe(res => {
                this.status = "REOPEN"
                this.isClose = true;
                this.route.navigate(['/tickets'])
              })
            })
          })
        }
        if (this.dataHdr.status == "CLOSE") {
          this.activatedRoute.params.subscribe(res => {
            this.threadHdrService.findById(res.id).subscribe(res => {
              this.updateThreadStatus.id = res.data?.id
              this.updateThreadStatus.status = "REOPEN"
              this.updateThreadStatus.version = res.data?.version
              this.threadHdrService.update(this.updateThreadStatus).subscribe(res => {
                this.status = "CLOSE"
                this.route.navigate(['/tickets'])
              })
            })
          })
        }
        if (this.dataHdr.status == "REOPEN") {
          this.activatedRoute.params.subscribe(res => {
            this.threadHdrService.findById(res.id).subscribe(res => {
              this.updateThreadStatus.id = res.data?.id
              this.updateThreadStatus.status = "CLOSE"
              this.updateThreadStatus.version = res.data?.version
              this.threadHdrService.update(this.updateThreadStatus).subscribe(res => {
                this.status = "REOPEN"
                this.isClose = true;
                this.route.navigate(['/tickets'])
              })
            })
          })
        }
      },
      reject: () => {

      }
    });
  }
}
