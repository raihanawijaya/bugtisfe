import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadHeaderUpdateComponent } from './thread-header-update.component';

describe('ThreadHeaderUpdateComponent', () => {
  let component: ThreadHeaderUpdateComponent;
  let fixture: ComponentFixture<ThreadHeaderUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThreadHeaderUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadHeaderUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
