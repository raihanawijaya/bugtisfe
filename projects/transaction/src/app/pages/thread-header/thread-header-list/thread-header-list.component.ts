import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { AuthService } from 'projects/ticketing/src/app/services/auth.service';
import { FindAllThreadHdrResData } from '../../../models/thread-header/find-all-thread-hdr-res-data';
import { ThreadHeaderService } from '../../../services/thread-header.service';

@Component({
  selector: 'app-thread-header-list',
  templateUrl: './thread-header-list.component.html',
  styleUrls: ['./thread-header-list.component.css'],
  providers: [ConfirmationService]
})
export class ThreadHeaderListComponent implements OnInit {

  isAccount?: boolean

  constructor(private threadHdrService: ThreadHeaderService, private route: Router,
    private title: Title, private confirmationService: ConfirmationService,
    private authService: AuthService) { }

  data: FindAllThreadHdrResData[] = []

  ngOnInit(): void {
    this.initData()
    this.title.setTitle('Thread Header')

    if (this.authService.getProfile().roleCode != 'ACC') {
      this.isAccount = true
    }
  }

  initData(): void {
    this.threadHdrService.findAll().toPromise().then(res => {
      this.data = res.data!
    })
  }

  view(id?: number): void {
    this.route.navigate(['/tickets/dtl/' + id])
  }
}
