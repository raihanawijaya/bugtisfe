import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadHeaderListComponent } from './thread-header-list.component';

describe('ThreadHeaderListComponent', () => {
  let component: ThreadHeaderListComponent;
  let fixture: ComponentFixture<ThreadHeaderListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThreadHeaderListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadHeaderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
