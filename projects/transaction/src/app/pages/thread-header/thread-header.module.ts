import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThreadHeaderCreateComponent } from './thread-header-create/thread-header-create.component';
import { ThreadHeaderListComponent } from './thread-header-list/thread-header-list.component';
import { ThreadHeaderUpdateComponent } from './thread-header-update/thread-header-update.component';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ThreadHeaderRoutingModule } from './thread-header-routing.module';
import { DropdownModule } from 'primeng/dropdown';
import { BadgeModule } from 'primeng/badge';

@NgModule({
  declarations: [
    ThreadHeaderCreateComponent,
    ThreadHeaderListComponent,
    ThreadHeaderUpdateComponent
  ],
  imports: [
    CommonModule,
    ThreadHeaderRoutingModule,
    TableModule,
    FormsModule,
    ButtonModule,
    InputTextModule,
    RadioButtonModule,
    DropdownModule,
    BadgeModule
  ],
  exports: [
    ThreadHeaderCreateComponent,
    ThreadHeaderListComponent,
    ThreadHeaderUpdateComponent
  ]
})
export class ThreadHeaderModule { }
