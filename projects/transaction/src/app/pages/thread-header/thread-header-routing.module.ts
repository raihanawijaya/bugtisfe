import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ThreadHeaderCreateComponent } from './thread-header-create/thread-header-create.component';
import { ThreadHeaderListComponent } from './thread-header-list/thread-header-list.component';
import { ThreadHeaderUpdateComponent } from './thread-header-update/thread-header-update.component';

const routes : Routes = [
  {
    path : "new",
    component: ThreadHeaderCreateComponent,
  },
  {
    path : "",
    component: ThreadHeaderListComponent,
  },
  {
    path : "edit",
    component: ThreadHeaderUpdateComponent,
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ThreadHeaderRoutingModule { }
