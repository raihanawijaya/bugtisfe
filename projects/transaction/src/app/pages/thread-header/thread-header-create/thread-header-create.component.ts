import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FindAllProductByIdAccountResponse } from 'projects/master/src/app/models/products/find-all-product-by-id-account-res';
import { FindAllProductByIdAccountResponseData } from 'projects/master/src/app/models/products/find-all-product-by-id-account-res-data';
import { FindAllProductResponseData } from 'projects/master/src/app/models/products/find-all-product-res-data';
import { FindAllStatusResponseData } from 'projects/master/src/app/models/status/find-all-status-res-data';
import { ProductService } from 'projects/master/src/app/services/product.service';
import { StatusService } from 'projects/master/src/app/services/status.service';
import { UserService } from 'projects/ticketing/src/app/services/user.service';
import { InsertThreadHdrReq } from '../../../models/thread-header/insert-thread-hdr-req';
import { ThreadHeaderService } from '../../../services/thread-header.service';

@Component({
  selector: 'app-thread-header-create',
  templateUrl: './thread-header-create.component.html',
  styleUrls: ['./thread-header-create.component.css']
})
export class ThreadHeaderCreateComponent implements OnInit {

  file: any;

  constructor(private title: Title, private threadHdrService: ThreadHeaderService,
    private productService: ProductService, private statusService: StatusService,
    private userService: UserService, private route: Router,
  ) { }

  threadHdrReq = new InsertThreadHdrReq();
  dataProduct: FindAllProductByIdAccountResponseData[] = []
  dataStatus: FindAllStatusResponseData[] = []

  ngOnInit(): void {
    this.title.setTitle('Thread Create')
    this.products()
    this.status()
  }

  products(): void {
    this.productService.findByIdAccount().toPromise().then(res => {
      this.dataProduct = res.data!
    })
  }

  status(): void {
    this.statusService.findAll().toPromise().then(res => {
      this.dataStatus = res.data!
    })
  }

  fileChange(data: any): void {
    if (data.files && data.files[0]) {
      this.file = data.files[0]
    }
  }

  onClick(): void {
    this.threadHdrService.insert(this.threadHdrReq, this.file).subscribe(res => {
      this.route.navigate(['/tickets'])
    })
  }


}
