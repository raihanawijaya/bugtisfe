import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadHeaderCreateComponent } from './thread-header-create.component';

describe('ThreadHeaderCreateComponent', () => {
  let component: ThreadHeaderCreateComponent;
  let fixture: ComponentFixture<ThreadHeaderCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThreadHeaderCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadHeaderCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
