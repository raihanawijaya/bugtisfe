import { InsertThreadHdrResData } from "./insert-thread-hdr-res-data"

export class InsertThreadHdrRes {
    msg?: string
    data?: InsertThreadHdrResData
}