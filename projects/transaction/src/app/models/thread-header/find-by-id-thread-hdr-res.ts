import { FindByIdThreadHdrResData } from "./find-by-id-thread-hdr-res-data"

export class FindByIdThreadHdrRes {
    msg?: string
    data?: FindByIdThreadHdrResData
}
