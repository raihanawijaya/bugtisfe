export class FindAllThreadHdrResData {
    id?: string
    title?: string
    description?: string
    agentName?: string
    product?: string
    status?: string
    noTicket?: string
    version?: number
}
