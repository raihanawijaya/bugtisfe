import { FindAllThreadHdrResData } from "./find-all-thread-hdr-res-data"

export class FindAllThreadHdrRes{
    msg?: string
    data?: FindAllThreadHdrResData[]
}
