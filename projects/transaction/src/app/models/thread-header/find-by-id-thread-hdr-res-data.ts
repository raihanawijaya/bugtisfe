export class FindByIdThreadHdrResData {
    id?: string
    title?: string
    noTicket?: string
    description?: string
    product?: string
    status?: string
    createdAt?: string
    file?: string
    version?: number
}
