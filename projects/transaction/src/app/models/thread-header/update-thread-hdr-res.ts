import { UpdateThreadHdrResData } from "./update-thread-hdr-res-data"

export class UpdateThreadHdrRes {
    msg?: string
    data?: UpdateThreadHdrResData
}