export class UpdateThreadHdrReq {
    id?: string
    status?: string
    version?: number
}