import { FindAllThreadDtlResData } from "./find-all-thread-dtl-res-data"

export class FindAllThreadDtlRes {
    msg?: string
    data?: FindAllThreadDtlResData[]
}
