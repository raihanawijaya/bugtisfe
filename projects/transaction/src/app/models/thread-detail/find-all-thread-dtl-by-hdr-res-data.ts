export class FindAllThreadDtlByHdrResData {
    id?: string
    msg?: string
    idThreadHdr?: string
    threadHdr?: string
    user?: string
    createdAt?: string
    file?: string
    isActive?: boolean
    version?: number
}