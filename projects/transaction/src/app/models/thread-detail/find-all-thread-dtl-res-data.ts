export class FindAllThreadDtlResData {
    id?: string
    msg?: string
    threadHdr?: string
    user?: string
    createdAt?: string
    isActive?: boolean
    version?: number
}
