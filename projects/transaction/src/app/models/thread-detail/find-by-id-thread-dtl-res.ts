import { FindByIdThreadDtlResData } from "./find-by-id-thread-dtl-res-data"

export class FindByIdThreadDtlRes {
    msg?: string
    data?: FindByIdThreadDtlResData
}
