import { InsertThreadDtlResData } from "./insert-thread-dtl-res-data"

export class InsertThreadDtlRes {
    msg?: string
    data?: InsertThreadDtlResData
}