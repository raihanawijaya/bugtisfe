export class FindByIdThreadDtlResData {
    id?: string
    msg?: string
    threadHdr?: string
    user?: string
    isActive?: string
    version?: number
}
