import { FindAllThreadDtlByHdrResData } from "./find-all-thread-dtl-by-hdr-res-data"

export class FindAllThreadDtlByHdrRes {
    msg?: string
    data?: FindAllThreadDtlByHdrResData[]
}