import { TestBed } from '@angular/core/testing';

import { ThreadDetailService } from './thread-detail.service';

describe('ThreadDetailService', () => {
  let service: ThreadDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThreadDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
