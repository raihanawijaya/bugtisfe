import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'projects/master/src/app/constant/general-constant';
import { Observable } from 'rxjs';
import { FindAllThreadDtlByHdrRes } from '../models/thread-detail/find-all-thread-dtl-by-hdr-res';
import { FindAllThreadDtlRes } from '../models/thread-detail/find-all-thread-dtl-res';
import { FindByIdThreadDtlRes } from '../models/thread-detail/find-by-id-thread-dtl-res';
import { InsertThreadDtlReq } from '../models/thread-detail/insert-thread-dtl-req';
import { InsertThreadDtlRes } from '../models/thread-detail/insert-thread-dtl-res';

@Injectable({
  providedIn: 'root'
})
export class ThreadDetailService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<FindAllThreadDtlRes> {
    return this.http.get(`${BASE_URL}/thread-dtl`)
  }

  findById(id: number): Observable<FindByIdThreadDtlRes> {
    return this.http.get(`${BASE_URL}/thread-dtl/${id}`)
  }

  findByHdr(id: number): Observable<FindAllThreadDtlByHdrRes> {
    return this.http.get(`${BASE_URL}/thread-dtl/hdr/${id}`)
  }

  insert(body: InsertThreadDtlReq, file: any): Observable<InsertThreadDtlRes> {
    let formData = new FormData();
    formData.append("threadDtl", JSON.stringify(body))
    formData.append("file", file)
    return this.http.post(`${BASE_URL}/thread-dtl`, formData)
  }
}
