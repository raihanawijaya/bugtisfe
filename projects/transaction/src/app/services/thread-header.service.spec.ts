import { TestBed } from '@angular/core/testing';

import { ThreadHeaderService } from './thread-header.service';

describe('ThreadHeaderService', () => {
  let service: ThreadHeaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThreadHeaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
