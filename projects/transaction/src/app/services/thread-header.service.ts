import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'projects/master/src/app/constant/general-constant';
import { Observable } from 'rxjs';
import { FindAllThreadHdrRes } from '../models/thread-header/find-all-thread-hdr-res';
import { FindByIdThreadHdrRes } from '../models/thread-header/find-by-id-thread-hdr-res';
import { InsertThreadHdrReq } from '../models/thread-header/insert-thread-hdr-req';
import { InsertThreadHdrRes } from '../models/thread-header/insert-thread-hdr-res';
import { UpdateThreadHdrReq } from '../models/thread-header/update-thread-hdr-req';
import { UpdateThreadHdrRes } from '../models/thread-header/update-thread-hdr-res';

@Injectable({
  providedIn: 'root'
})
export class ThreadHeaderService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<FindAllThreadHdrRes> {
    return this.http.get(`${BASE_URL}/thread-hdr`)
  }

  findById(id: number): Observable<FindByIdThreadHdrRes> {
    return this.http.get(`${BASE_URL}/thread-hdr/${id}`)
  }

  insert(body: InsertThreadHdrReq, file: any): Observable<InsertThreadHdrRes> {
    let formData = new FormData();
    formData.append("thread", JSON.stringify(body))
    formData.append("file", file)
    return this.http.post(`${BASE_URL}/thread-hdr`, formData)
  }

  update(body: UpdateThreadHdrReq): Observable<UpdateThreadHdrRes> {
    return this.http.patch(`${BASE_URL}/thread-hdr`, body)
  }
}
