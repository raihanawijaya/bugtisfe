import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    loadChildren: () => import('./pages/role/role.module').then(r => r.RoleModule)
  },
  {
    path: "",
    loadChildren: () => import('./pages/status/status.module').then(r => r.StatusModule)
  },
  {
    path: "",
    loadChildren: () => import('./pages/company/company.module').then(r => r.CompanyModule)
  },
  {
    path: "",
    loadChildren: () => import('./pages/product/product.module').then(r => r.ProductModule)
  },
  {
    path: "",
    loadChildren: () => import('./pages/user/user.module').then(r => r.UserModule)
  },
  {
    path: "",
    loadChildren: () => import('./pages/agent-list/agent-list.module').then(r => r.AgentListModule)
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
