import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from '../constant/general-constant';
import { DeleteStatusResponse } from '../models/status/delete-status-res';
import { FindAllStatusResponse } from '../models/status/find-all-status-res';
import { FindByIdStatusResponse } from '../models/status/find-by-id-status-res';
import { InsertStatusRequest } from '../models/status/insert-status-req';
import { InsertStatusResponse } from '../models/status/insert-status-res';
import { UpdateStatusRequest } from '../models/status/update-status-req';
import { UpdateStatusResponse } from '../models/status/update-status-res';

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<FindAllStatusResponse> {
    return this.http.get(`${BASE_URL}/status`)
  }

  findById(id: number): Observable<FindByIdStatusResponse> {
    return this.http.get(`${BASE_URL}/status/${id}`)
  }

  insert(body: InsertStatusRequest): Observable<InsertStatusResponse> {
    return this.http.post(`${BASE_URL}/status`, body)
  }

  update(body: UpdateStatusRequest): Observable<UpdateStatusResponse> {
    return this.http.put(`${BASE_URL}/status`, body)
  }

  delete(id: number): Observable<DeleteStatusResponse> {
    return this.http.delete(`${BASE_URL}/status/${id}`)
  }

}
