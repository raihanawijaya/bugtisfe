import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from '../constant/general-constant';
import { DeleteAccountRes } from '../models/accounts/delete-account-res';
import { FindAllAccountRes } from '../models/accounts/find-all-account-res';
import { FindByIdAccountRes } from '../models/accounts/find-by-id-account-res';
import { InsertAccountReq } from '../models/accounts/insert-account-req';
import { InsertAccountRes } from '../models/accounts/insert-account-res';
import { UpdateAccountReq } from '../models/accounts/update-account-req';
import { UpdateAccountRes } from '../models/accounts/update-account-res';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<FindAllAccountRes> {
    return this.http.get(`${BASE_URL}/accounts`)
  }

  findById(id: number): Observable<FindByIdAccountRes> {
    return this.http.get(`${BASE_URL}/accounts/${id}`)
  }

  insert(body: InsertAccountReq): Observable<InsertAccountRes> {
    return this.http.post(`${BASE_URL}/accounts`, body)
  }

  update(body: UpdateAccountReq): Observable<UpdateAccountRes> {
    return this.http.put(`${BASE_URL}/accounts`, body)
  }

  delete(id: number): Observable<DeleteAccountRes> {
    return this.http.delete(`${BASE_URL}/accounts/${id}`)
  }

}
