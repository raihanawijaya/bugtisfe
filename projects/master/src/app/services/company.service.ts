import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from '../constant/general-constant';
import { DeleteCompanyResponse } from '../models/companies/delete-company-res';
import { FindAllCompanyResponse } from '../models/companies/find-all-company-res';
import { FindByIdCompanyResponse } from '../models/companies/find-by-id-company-res';
import { InsertCompanyRequest } from '../models/companies/insert-company-req';
import { InsertCompanyResponse } from '../models/companies/insert-company-res';
import { UpdateCompanyRequest } from '../models/companies/update-company-req';
import { UpdateCompanyResponse } from '../models/companies/update-company-res';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<FindAllCompanyResponse> {
    return this.http.get(`${BASE_URL}/companies`)
  }

  findById(id: number): Observable<FindByIdCompanyResponse> {
    return this.http.get(`${BASE_URL}/companies/${id}`)
  }

  insert(body: InsertCompanyRequest): Observable<InsertCompanyResponse> {
    return this.http.post(`${BASE_URL}/companies`, body)
  }

  update(body: UpdateCompanyRequest): Observable<UpdateCompanyResponse> {
    return this.http.put(`${BASE_URL}/companies`, body)
  }

  delete(id: number): Observable<DeleteCompanyResponse> {
    return this.http.delete(`${BASE_URL}/companies/${id}`)
  }

}
