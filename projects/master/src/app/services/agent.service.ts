import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from '../constant/general-constant';
import { DeleteAgentRes } from '../models/agent/delete-agent-res';
import { FindAgentByIdRes } from '../models/agent/find-agent-by-id-res';
import { FindAllAgentRes } from '../models/agent/find-all-agent-res';
import { InsertAgentReq } from '../models/agent/insert-agent-req';
import { InsertAgentRes } from '../models/agent/insert-agent-res';
import { UpdateAgentReq } from '../models/agent/update-agent-req';
import { UpdateAgentRes } from '../models/agent/update-agent-res';

@Injectable({
  providedIn: 'root'
})
export class AgentService {

  constructor(private http: HttpClient) { }

  findAll() : Observable<FindAllAgentRes> {
    return this.http.get(`${BASE_URL}/agents`)
  }

  findById(id: number): Observable<FindAgentByIdRes> {
    return this.http.get(`${BASE_URL}/agents/${id}`)
  }

  insert(body: InsertAgentReq): Observable<InsertAgentRes> {
    return this.http.post(`${BASE_URL}/agents`, body)
  }

  update(body: UpdateAgentReq): Observable<UpdateAgentRes> {
    return this.http.put(`${BASE_URL}/agents`, body)
  }

  delete (id : number): Observable<DeleteAgentRes> {
    return this.http.delete(`${BASE_URL}/agents/${id}`)
  }

}
