import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from '../constant/general-constant';
import { DeletePicRes } from '../models/pic/delete-pic-res';
import { FindAllPicByAccountRes } from '../models/pic/find-all-pic-by-account-res';
import { FindAllPicByAccountResData } from '../models/pic/find-all-pic-by-account-res-data';
import { FindAllPicRes } from '../models/pic/find-all-pic-res';
import { FindByIdPicRes } from '../models/pic/find-by-id-pic-res';
import { InsertPicReq } from '../models/pic/insert-pic-req';
import { InsertPicRes } from '../models/pic/insert-pic-res';
import { UpdatePicReq } from '../models/pic/update-pic-req';
import { UpdatePicRes } from '../models/pic/update-pic-res';

@Injectable({
  providedIn: 'root'
})
export class PicService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<FindAllPicRes> {
    return this.http.get(`${BASE_URL}/pic`)
  }

  findById(id: string): Observable<FindByIdPicRes> {
    return this.http.get(`${BASE_URL}/pic/${id}`)
  }

  insert(body: InsertPicReq): Observable<InsertPicRes> {
    return this.http.post(`${BASE_URL}/pic`, body)
  }

  update(body: UpdatePicReq): Observable<UpdatePicRes> {
    return this.http.put(`${BASE_URL}/pic`, body)
  }

  delete(id: string): Observable<DeletePicRes> {
    return this.http.delete(`${BASE_URL}/pic/${id}`)
  }

  findByAccount(id: string): Observable<FindAllPicByAccountRes> {
    return this.http.get(`${BASE_URL}/pic/acc/${id}`)
  }

}
