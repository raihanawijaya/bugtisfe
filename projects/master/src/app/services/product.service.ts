import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from '../constant/general-constant';
import { DeleteProductResponse } from '../models/products/delete-product-res';
import { FindAllProductByIdAccountResponse } from '../models/products/find-all-product-by-id-account-res';
import { FindAllProductResponse } from '../models/products/find-all-product-res';
import { FindByIdProductResponse } from '../models/products/find-by-id-product-res';
import { InsertProductRequest } from '../models/products/insert-product-req';
import { InsertProductResponse } from '../models/products/insert-product-res';
import { UpdateProductRequest } from '../models/products/update-product-req';
import { UpdateProductResponse } from '../models/products/update-product-res';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<FindAllProductResponse> {
    return this.http.get(`${BASE_URL}/products`)
  }

  findByIdAccount(): Observable<FindAllProductByIdAccountResponse> {
    return this.http.get(`${BASE_URL}/products/account`)
  }

  findById(id: number): Observable<FindByIdProductResponse> {
    return this.http.get(`${BASE_URL}/products/${id}`)
  }

  insert(body: InsertProductRequest): Observable<InsertProductResponse> {
    return this.http.post(`${BASE_URL}/products`, body)
  }

  update(body: UpdateProductRequest): Observable<UpdateProductResponse> {
    return this.http.put(`${BASE_URL}/products`, body)
  }

  delete(id: number): Observable<DeleteProductResponse> {
    return this.http.delete(`${BASE_URL}/products/${id}`)
  }

}
