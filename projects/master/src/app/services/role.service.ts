import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from '../constant/general-constant';
import { DeleteRoleResponse } from '../models/roles/delete-role-res';
import { FindAllRoleResponse } from '../models/roles/find-all-role-res';
import { FindByIdRoleResponse } from '../models/roles/find-by-id-role-res';
import { InsertRoleRequest } from '../models/roles/insert-role-req';
import { InsertRoleResponse } from '../models/roles/insert-role-res';
import { UpdateRoleRequest } from '../models/roles/update-role-req';
import { UpdateRoleResponse } from '../models/roles/update-role-res';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<FindAllRoleResponse> {
    return this.http.get(`${BASE_URL}/roles`)
  }

  findById(id: string): Observable<FindByIdRoleResponse> {
    return this.http.get(`${BASE_URL}/roles/${id}`)
  }

  insert(body: InsertRoleRequest): Observable<InsertRoleResponse> {
    return this.http.post(`${BASE_URL}/roles`, body)
  }

  update(body: UpdateRoleRequest): Observable<UpdateRoleResponse> {
    return this.http.put(`${BASE_URL}/roles`, body)
  }

  delete(id: string): Observable<DeleteRoleResponse> {
    return this.http.delete(`${BASE_URL}/roles/${id}`)
  }

}
