import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from '../constant/general-constant';
import { UpdateAccountRes } from '../models/accounts/update-account-res';
import { DeleteUserRes } from '../models/user/delete-user-res';
import { FindAllUserRes } from '../models/user/find-all-user-res';
import { FindByLoginRes } from '../models/user/find-by-login-res';
import { FindUserByIdRes } from '../models/user/find-user-by-id-res';
import { InsertUserReq } from '../models/user/insert-user-req';
import { InsertUserRes } from '../models/user/insert-user-res';
import { UpdatePassUsersReq } from '../models/user/update-pass-user-req';
import { UpdatePassUsersRes } from '../models/user/update-pass-user-res';
import { UpdateUserReq } from '../models/user/update-user-req';
import { UpdateUserRes } from '../models/user/update-user-res';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  findAll() : Observable<FindAllUserRes> {
    return this.http.get(`${BASE_URL}/users`)
  }

  findAllAgent() : Observable<FindAllUserRes> {
    return this.http.get(`${BASE_URL}/users/agent`)
  }

  findAllCustomer() : Observable<FindAllUserRes> {
    return this.http.get(`${BASE_URL}/users/customer`)
  }

  findById(id: string): Observable<FindUserByIdRes> {
    return this.http.get(`${BASE_URL}/users/${id}`)
  }

  findByLogin(id: string): Observable<FindByLoginRes>{
    return this.http.get(`${BASE_URL}/users/login/${id}`)
  }

  insert(body: InsertUserReq): Observable<InsertUserRes> {
    return this.http.post(`${BASE_URL}/users`, body)
  }

  update(body: UpdateUserReq): Observable<UpdateUserRes> {
    return this.http.put(`${BASE_URL}/users`, body)
  }

  delete (id : string): Observable<DeleteUserRes> {
    return this.http.delete(`${BASE_URL}/users/${id}`)
  }

  updatePass(body: UpdatePassUsersReq): Observable<UpdatePassUsersRes> {
    return this.http.patch(`${BASE_URL}/users`, body)
  }

}
