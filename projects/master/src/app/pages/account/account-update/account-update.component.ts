import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'projects/ticketing/src/app/services/auth.service';
import { UpdateAccountReq } from '../../../models/accounts/update-account-req';
import { FindAllCompanyResponseData } from '../../../models/companies/find-all-company-res-data';
import { FindAllUserResData } from '../../../models/user/find-all-user-res-data';
import { AccountService } from '../../../services/account.service';
import { CompanyService } from '../../../services/company.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-account-update',
  templateUrl: './account-update.component.html',
  styleUrls: ['./account-update.component.css']
})
export class AccountUpdateComponent implements OnInit {

  company : FindAllCompanyResponseData[] = []

  user : FindAllUserResData[] = []

  account = new UpdateAccountReq()

  constructor(private title: Title, private route: Router, private accountService: AccountService, 
    private activatedRoute: ActivatedRoute, private companyService : CompanyService, 
    private userService : UserService, private authService : AuthService) { }

  ngOnInit(): void {
    this.title.setTitle('Edit Account')

    this.companyService.findAll().subscribe(response => {
      this.company = response.data!
    })

    if (this.authService.getProfile().roleCode == 'ADM') {
      this.userService.findAllCustomer().subscribe(respon => {
        this.user = respon.data!
      })
    } else if (this.authService.getProfile().roleCode == 'CST') {
      this.userService.findAll().subscribe(respon => {
        this.user = respon.data!
      })
    } 

    this.activatedRoute.params.subscribe(res => {
      this.accountService.findById(res.id).subscribe(res => {
        this.account.id = res.data?.id
        this.account.company = res.data?.company
        this.account.user = res.data?.user
        this.account.version = res.data?.version
        this.account.isActive = res.data?.isActive
      })
    })
  }

  onClick() : void {
    this.accountService.update(this.account).subscribe(res => {
      this.route.navigate(['accounts'])
    })
  }

}
