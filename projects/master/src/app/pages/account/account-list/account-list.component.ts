import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { FindAllAccountResData } from '../../../models/accounts/find-all-account-res-data';
import { AccountService } from '../../../services/account.service';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css'],
  providers: [ConfirmationService]
})
export class AccountListComponent implements OnInit {

  constructor(private accountService: AccountService, private route: Router, private title: Title, private confirmationService: ConfirmationService) { }

  data: FindAllAccountResData[] = []

  confirm(id?: number) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.accountService.delete(id!).subscribe(res => {
          this.initData()
        })
      },
      reject: () => {
        this.initData()
      }
    });
  }

  ngOnInit(): void {
    this.initData()
    this.title.setTitle('Account')
  }

  initData(): void {
    this.accountService.findAll().toPromise().then(res => {
      this.data = res.data!
    })
  }

  update(id?: number): void {
    this.route.navigate(['/accounts/edit/' + id])
  }

  view(id?: number):void{
    this.route.navigate(['/pic/'+id])
  }

}
