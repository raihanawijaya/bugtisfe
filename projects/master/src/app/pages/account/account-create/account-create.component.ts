import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { InsertAccountReq } from '../../../models/accounts/insert-account-req';
import { FindAllCompanyResponseData } from '../../../models/companies/find-all-company-res-data';
import { AccountService } from '../../../services/account.service';
import { CompanyService } from '../../../services/company.service';
import { FindAllUserResData } from '../../../models/user/find-all-user-res-data';
import { AuthService } from 'projects/ticketing/src/app/services/auth.service';

@Component({
  selector: 'app-account-create',
  templateUrl: './account-create.component.html',
  styleUrls: ['./account-create.component.css']
})
export class AccountCreateComponent implements OnInit {

  company : FindAllCompanyResponseData[] = []

  user : FindAllUserResData[] = []

  account = new InsertAccountReq()

  constructor(private title: Title, private route : Router, private accountService : AccountService,
    private companyService : CompanyService, private userService : UserService, private authService : AuthService) { }

  ngOnInit(): void {
    this.title.setTitle('Add Account')

    
    this.companyService.findAll().subscribe(response => {
      this.company = response.data!
    })

    if (this.authService.getProfile().roleCode == 'ADM') {
      this.userService.findAllCustomer().subscribe(respon => {
        this.user = respon.data!
      })
    } else if (this.authService.getProfile().roleCode == 'CST') {
      this.userService.findAll().subscribe(respon => {
        this.user = respon.data!
      })
    } 
    
  }

  onClick() : void {
    this.accountService.insert(this.account).subscribe(res => {
      this.route.navigate(['accounts'])
    })
  }

}
