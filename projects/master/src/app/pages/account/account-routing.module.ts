import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AccountCreateComponent } from './account-create/account-create.component';
import { AccountListComponent } from './account-list/account-list.component';
import { AccountUpdateComponent } from './account-update/account-update.component';
import { CanAccessService } from 'projects/ticketing/src/app/services/can-access.service';
import { CanAuthorizeService } from 'projects/ticketing/src/app/services/can-authorize.service';

const routes : Routes = [
  {
    path : "new",
    component: AccountCreateComponent,
  },
  {
    path : "",
    component: AccountListComponent,
  },
  {
    path : "edit/:id",
    component: AccountUpdateComponent,
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AccountRoutingModule { }
