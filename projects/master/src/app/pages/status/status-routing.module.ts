import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StatusCreateComponent } from './status-create/status-create.component';
import { StatusListComponent } from './status-list/status-list.component';
import { StatusUpdateComponent } from './status-update/status-update.component';
import { CanAuthorizeService } from 'projects/ticketing/src/app/services/can-authorize.service';
import { CanAccessService } from 'projects/ticketing/src/app/services/can-access.service';

const routes : Routes = [
  {
    path : "new",
    component: StatusCreateComponent,
  },
  {
    path : "",
    component: StatusListComponent,
  },
  {
    path : "edit/:id",
    component: StatusUpdateComponent
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class StatusRoutingModule { }
