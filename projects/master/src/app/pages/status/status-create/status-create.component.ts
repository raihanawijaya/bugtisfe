import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { InsertStatusRequest } from '../../../models/status/insert-status-req';
import { StatusService } from '../../../services/status.service';

@Component({
  selector: 'app-status-create',
  templateUrl: './status-create.component.html',
  styleUrls: ['./status-create.component.css']
})
export class StatusCreateComponent implements OnInit {

  status = new InsertStatusRequest()

  constructor(private title: Title, private statusService : StatusService, private route : Router) { }

  ngOnInit(): void {
    this.title.setTitle('Add Status')
  }

  onClick(): void {
    this.statusService.insert(this.status).subscribe(res => {
      this.route.navigate(['status'])
    })
  }

}
