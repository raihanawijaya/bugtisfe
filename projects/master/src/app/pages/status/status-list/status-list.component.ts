import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { FindAllStatusResponseData } from '../../../models/status/find-all-status-res-data';
import { StatusService } from '../../../services/status.service';

@Component({
  selector: 'app-status-list',
  templateUrl: './status-list.component.html',
  styleUrls: ['./status-list.component.css'],
  providers: [ConfirmationService]
})
export class StatusListComponent implements OnInit {

  constructor(private statusService: StatusService, private route: Router,
    private title: Title, private confirmationService: ConfirmationService) { }

  data: FindAllStatusResponseData[] = []

  confirm(id?: number) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.statusService.delete(id!).subscribe(res => {
          this.initData()
        })
      },
      reject: () => {
        this.initData()
      }
    });
  }

  ngOnInit(): void {
    this.title.setTitle('Status')
    this.initData()
  }

  initData(): void {
    this.statusService.findAll().toPromise().then(res => {
      this.data = res.data!
    })
  }

  update(id?: number): void {
    this.route.navigate(['/status/edit/' + id])
  }

}
