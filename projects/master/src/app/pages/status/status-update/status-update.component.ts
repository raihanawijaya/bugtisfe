import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { UpdateStatusRequest } from '../../../models/status/update-status-req';
import { StatusService } from '../../../services/status.service';

@Component({
  selector: 'app-status-update',
  templateUrl: './status-update.component.html',
  styleUrls: ['./status-update.component.css']
})
export class StatusUpdateComponent implements OnInit {

  status = new UpdateStatusRequest()

  constructor(private title: Title, private statusService: StatusService,
    private route: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.title.setTitle('Edit Status')

    this.activatedRoute.params.subscribe(res => {
      this.statusService.findById(res.id).subscribe(res => {
        this.status.id = res.data?.id
        this.status.code = res.data?.code
        this.status.name = res.data?.name
        this.status.version = res.data?.version
        this.status.isActive = res.data?.isActive
      })
    })
  }

  onClick() : void {
    this.statusService.update(this.status).subscribe(res => {
      this.route.navigate(['status'])
    })
  }

}
