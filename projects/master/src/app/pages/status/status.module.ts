import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { StatusCreateComponent } from './status-create/status-create.component';
import { StatusListComponent } from './status-list/status-list.component';
import { StatusRoutingModule } from './status-routing.module';
import { StatusUpdateComponent } from './status-update/status-update.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';



@NgModule({
  declarations: [
    StatusListComponent,
    StatusCreateComponent,
    StatusUpdateComponent
  ],
  imports: [
    CommonModule,
    StatusRoutingModule,
    FormsModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    RadioButtonModule,
    ConfirmDialogModule,
  ],
  exports: [
    StatusListComponent,
    StatusCreateComponent,
    StatusUpdateComponent
  ]
})
export class StatusModule { }
