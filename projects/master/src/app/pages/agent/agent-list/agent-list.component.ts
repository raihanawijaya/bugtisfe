import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { FindAllAgentResData } from '../../../models/agent/find-all-agent-res-data';
import { AgentService } from '../../../services/agent.service';

@Component({
  selector: 'app-agent-list',
  templateUrl: './agent-list.component.html',
  styleUrls: ['./agent-list.component.css'],
  providers: [ConfirmationService]
})
export class AgentListComponent implements OnInit {

  constructor(private agentService: AgentService, private route: Router,
    private title: Title, private confirmationService: ConfirmationService) { }

  data: FindAllAgentResData[] = []

  confirm(id?: number) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.agentService.delete(id!).subscribe(res => {
          this.initData()
        })
      },
      reject: () => {
        this.initData()
      }
    });
  }

  ngOnInit(): void {
    this.initData()
    this.title.setTitle('Agent List')
  }

  initData(): void {
    this.agentService.findAll().toPromise().then(res => {
      this.data = res.data!
    })
  }

  update(id?: number): void {
    this.route.navigate(['/agents/edit/' + id])
  }
}
