import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { AgentCreateComponent } from './agent-create/agent-create.component';
import { AgentListComponent } from './agent-list/agent-list.component';
import { AgentRoutingModule } from './agent-routing.module';
import { AgentUpdateComponent } from './agent-update/agent-update.component';


@NgModule({
  declarations: [
    AgentCreateComponent,
    AgentListComponent,
    AgentUpdateComponent,
  ],
  imports: [
    CommonModule,
    AgentRoutingModule,
    FormsModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    RadioButtonModule,
    DropdownModule,
    ConfirmDialogModule
  ],
  exports : [
    AgentCreateComponent,
    AgentListComponent,
    AgentUpdateComponent,
  ]
})
export class AgentModule { }
