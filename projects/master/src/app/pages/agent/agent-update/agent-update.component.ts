import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'projects/ticketing/src/app/services/auth.service';
import { UpdateAgentReq } from '../../../models/agent/update-agent-req';
import { FindAllProductResponseData } from '../../../models/products/find-all-product-res-data';
import { FindAllUserResData } from '../../../models/user/find-all-user-res-data';
import { AgentService } from '../../../services/agent.service';
import { ProductService } from '../../../services/product.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-agent-update',
  templateUrl: './agent-update.component.html',
  styleUrls: ['./agent-update.component.css']
})
export class AgentUpdateComponent implements OnInit {

  data = new UpdateAgentReq()

  dataUser : FindAllUserResData[] = []

  dataProduct: FindAllProductResponseData[] = []

  status = ""

  constructor(private title: Title, private service : AgentService, private route : Router,
    private userService : UserService, private productService : ProductService,
    private authService: AuthService, private activatedRoute: ActivatedRoute) { }
 

   ngOnInit(): void {
    this.activatedRoute.params.subscribe(res=>{
      this.service.findById(res.id).subscribe(res=>{
        this.data.id = res.data?.id
        this.data.product = res.data?.product
        this.data.user = res.data?.user
        this.data.version = res.data?.version
        this.data.isActive = res.data?.isActive
      })
    })

    this.userService.findAllAgent().toPromise().then(res => {
      this.dataUser = res.data!
    })
    
    this.productService.findAll().toPromise().then(res => {
      this.dataProduct = res.data!
    })
  }

  onClick(): void {
    this.service.update(this.data).subscribe(res=>{
      this.route.navigate(['/agents'])
    })
  }

}

