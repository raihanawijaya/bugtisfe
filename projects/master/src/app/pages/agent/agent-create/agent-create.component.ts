import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from 'projects/ticketing/src/app/services/auth.service';
import { InsertAgentReq } from '../../../models/agent/insert-agent-req';
import { FindAllProductResponseData } from '../../../models/products/find-all-product-res-data';
import { InsertListProduct } from '../../../models/products/insert-list-product';
import { listProductDetail } from '../../../models/products/list-product';
import { FindAllRoleResponseData } from '../../../models/roles/find-all-role-res-data';
import { FindAllUserResData } from '../../../models/user/find-all-user-res-data';
import { InsertUserReq } from '../../../models/user/insert-user-req';
import { AgentService } from '../../../services/agent.service';
import { ProductService } from '../../../services/product.service';
import { RoleService } from '../../../services/role.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-agent-create',
  templateUrl: './agent-create.component.html',
  styleUrls: ['./agent-create.component.css']
})

export class AgentCreateComponent implements OnInit {

    data = new InsertAgentReq()

    dataUser : FindAllUserResData[] = []

    dataProduct: FindAllProductResponseData[] = []

    listData: listProductDetail[] = []
    listModel = new listProductDetail();
    insertDataProduct = new InsertListProduct();
  
    constructor(private title: Title, private service : AgentService, private route : Router,
      private userService : UserService, private productService : ProductService) { }
  
    ngOnInit(): void {
      this.title.setTitle('Add Agent List')

      this.userService.findAllAgent().toPromise().then(res => {
        this.dataUser = res.data!
      })

      this.productService.findAll().toPromise().then(res => {
        this.dataProduct = res.data!
      })
    }
  
    onClick(): void {
      this.service.insert(this.data).subscribe(res => {
        this.route.navigate(['agents'])
      })
    }

    productChange(data: any): void {
      this.listModel = new listProductDetail();
      this.productService.findById(data.value).subscribe(res => {
        this.listModel.product = res.data?.name
        this.listData.push(this.listModel)
      })
    }

    onSave(): void {
      // this.listData.push(this.listModel)
    }

    addList(): void {
      this.insertDataProduct.detailProduct = []
      for (let i = 0; i < this.listData.length; i++) {
        let productDetail = new listProductDetail()
        productDetail.product = this.listData[i].product
        this.insertDataProduct.detailProduct?.push(productDetail);
  
      }
      // this.productService.insert(this.insertDataProduct).subscribe(res => {
      //   this.route.navigate(['/cart-list'])
      // })
    }

}
  