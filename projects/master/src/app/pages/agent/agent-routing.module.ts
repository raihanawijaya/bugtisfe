import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgentCreateComponent } from './agent-create/agent-create.component';
import { AgentListComponent } from './agent-list/agent-list.component';
import { AgentUpdateComponent } from './agent-update/agent-update.component';


const routes : Routes = [
  {
    path : "new",
    component: AgentCreateComponent,
  },
  {
    path : "",
    component: AgentListComponent,
  },
  {
    path : "edit/:id",
    component: AgentUpdateComponent,
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AgentRoutingModule { }
