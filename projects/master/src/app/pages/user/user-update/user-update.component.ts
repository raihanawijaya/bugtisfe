import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'projects/ticketing/src/app/services/auth.service';
import { FindAllRoleResponseData } from '../../../models/roles/find-all-role-res-data';
import { UpdateUserReq } from '../../../models/user/update-user-req';
import { RoleService } from '../../../services/role.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  data = new UpdateUserReq();

  dataRoles : FindAllRoleResponseData[] = []

  status = ""

  constructor(private route: Router, private service: UserService, private rolesService: RoleService, 
    private authService: AuthService, private activatedRoute: ActivatedRoute) { }

   ngOnInit(): void {
    this.activatedRoute.params.subscribe(res=>{
      this.service.findById(res.id).subscribe(res=>{
        this.data.id = res.data?.id
        this.data.name = res.data?.name
        this.data.email = res.data?.email
        this.data.role = res.data?.role
        this.data.version = res.data?.version
        this.data.isActive = res.data?.isActive
      })
    })

    this.rolesService.findAll().subscribe(res=>{
      this.dataRoles = res.data!
      console.log(res)
    })
  }


  onClick(): void {
    this.service.update(this.data).subscribe(res=>{
      this.route.navigate(['/users'])
    })
  }

}
