import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { FindAllUserResData } from '../../../models/user/find-all-user-res-data';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers: [ConfirmationService]
})
export class UserListComponent implements OnInit {

  constructor(private userService: UserService, private route: Router,
    private title: Title, private confirmationService: ConfirmationService) { }

  data: FindAllUserResData[] = []

  confirm(id?: string) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.userService.delete(id!).subscribe(res => {
          this.initData()
        })
      },
      reject: () => {
        this.initData()
      }
    });
  }

  ngOnInit(): void {
    this.initData()
    this.title.setTitle('User')
  }

  initData(): void {
    this.userService.findAll().toPromise().then(res => {
      this.data = res.data!
    })
  }

  update(id?: number): void {
    this.route.navigate(['/users/edit/' + id])
  }
}
