import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'projects/ticketing/src/app/services/auth.service';
import { FindAllRoleResponseData } from '../../../models/roles/find-all-role-res-data';
import { InsertUserReq } from '../../../models/user/insert-user-req';
import { RoleService } from '../../../services/role.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  dataRoles : FindAllRoleResponseData[] = []

  data = new InsertUserReq();

  status = ""

  constructor(private route: Router, private userService: UserService, private rolesService: RoleService, private authService: AuthService) { }

  ngOnInit(): void {
    this.rolesService.findAll().toPromise().then(res => {
      this.dataRoles = res.data!
      console.log(this.dataRoles)
    })

    // this.rolesService.findAll().subscribe(res=>{
    //   this.dataRoles = res.data!
    //   console.log(res)
    // })
  }

  onClick(): void {
    this.userService.insert(this.data).subscribe(res=>{
      this.route.navigate(['/users'])
    })
  }

}