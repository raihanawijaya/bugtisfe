import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserUpdateComponent } from './user-update/user-update.component';
import { CanAuthorizeService } from 'projects/ticketing/src/app/services/can-authorize.service';
import { CanAccessService } from 'projects/ticketing/src/app/services/can-access.service';

const routes : Routes = [
  {
    path : "new",
    component: UserCreateComponent,
  },
  {
    path : "",
    component: UserListComponent,
  },
  {
    path : "edit/:id",
    component: UserUpdateComponent,
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UserRoutingModule { }
