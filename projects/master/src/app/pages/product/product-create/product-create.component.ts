import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { InsertProductRequest } from '../../../models/products/insert-product-req';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {

  product = new InsertProductRequest()

  constructor(private title: Title, private productService : ProductService, private route : Router) { }

  ngOnInit(): void {
    this.title.setTitle('Add Product')
  }

  onClick(): void {
    this.productService.insert(this.product).subscribe(res => {
      this.route.navigate(['products'])
    })
  }

}
