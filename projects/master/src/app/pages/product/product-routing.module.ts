import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanAccessService } from 'projects/ticketing/src/app/services/can-access.service';
import { CanAuthorizeService } from 'projects/ticketing/src/app/services/can-authorize.service';
import { ProductCreateComponent } from './product-create/product-create.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductUpdateComponent } from './product-update/product-update.component';

const routes : Routes = [
  {
    path : "new",
    component: ProductCreateComponent,
  },
  {
    path : "",
    component: ProductListComponent,
  },
  {
    path : "edit/:id",
    component: ProductUpdateComponent,
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductRoutingModule { }
