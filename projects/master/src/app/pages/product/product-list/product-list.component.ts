import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { FindAllProductResponseData } from '../../../models/products/find-all-product-res-data';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  providers: [ConfirmationService]
})
export class ProductListComponent implements OnInit {

  constructor(private productService: ProductService, private route: Router,
    private title: Title, private confirmationService: ConfirmationService) { }

  data: FindAllProductResponseData[] = []

  confirm(id?: number) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.productService.delete(id!).subscribe(res => {
          this.initData()
        })
      },
      reject: () => {
        this.initData()
      }
    });
  }


  ngOnInit(): void {
    this.initData()
    this.title.setTitle('Product')
  }

  initData(): void {
    this.productService.findAll().toPromise().then(res => {
      this.data = res.data!
    })
  }

  update(id?: number): void {
    this.route.navigate(['/products/edit/' + id])
  }

}
