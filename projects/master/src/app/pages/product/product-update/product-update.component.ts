import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { UpdateProductRequest } from '../../../models/products/update-product-req';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {

  product = new UpdateProductRequest()

  constructor(private title: Title, private productService: ProductService,
    private route: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.title.setTitle('Edit Product')

    this.activatedRoute.params.subscribe(res => {
      this.productService.findById(res.id).subscribe(res => {
        this.product.id = res.data?.id
        this.product.code = res.data?.code
        this.product.name = res.data?.name
        this.product.version = res.data?.version
        this.product.isActive = res.data?.isActive
      })
    })
  }

  onClick() : void {
    this.productService.update(this.product).subscribe(res => {
      this.route.navigate(['products'])
    })
  }

}
