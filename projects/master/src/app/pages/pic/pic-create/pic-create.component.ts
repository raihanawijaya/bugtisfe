import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { InsertPicReq } from '../../../models/pic/insert-pic-req';
import { FindAllAccountResData } from '../../../models/accounts/find-all-account-res-data';
import { FindAllProductResponseData } from '../../../models/products/find-all-product-res-data';
import { PicService } from '../../../services/pic.service';
import { ProductService } from '../../../services/product.service';
import { AuthService } from 'projects/ticketing/src/app/services/auth.service';

@Component({
  selector: 'app-pic-create',
  templateUrl: './pic-create.component.html',
  styleUrls: ['./pic-create.component.css']
})
export class PicCreateComponent implements OnInit {

  account: FindAllAccountResData[] = []

  product: FindAllProductResponseData[] = []

  pic = new InsertPicReq()

  constructor(private title: Title, private route: Router, private picService: PicService,
    private productService: ProductService, private activatedRoute: ActivatedRoute,
    private location : Location, private authService: AuthService) { }

  ngOnInit(): void {
    this.title.setTitle('Add Account')


    this.activatedRoute.params.subscribe(res => {
      this.pic.account = res.id
    })

    
    if (this.authService.getProfile().roleCode == 'ADM') {
      this.productService.findAll().subscribe(respon => {
        this.product = respon.data!
      })
    } else {
      this.productService.findByIdAccount().subscribe(respon => {
        this.product = respon.data!
      })
    }

    
  }

  onClick(): void {
    this.picService.insert(this.pic).subscribe(res => {
      this.back()
    })
  }

  back() : void {
    this.location.back()
  }

}
