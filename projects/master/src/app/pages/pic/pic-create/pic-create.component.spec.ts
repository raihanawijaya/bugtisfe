import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicCreateComponent } from './pic-create.component';

describe('PicCreateComponent', () => {
  let component: PicCreateComponent;
  let fixture: ComponentFixture<PicCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PicCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PicCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
