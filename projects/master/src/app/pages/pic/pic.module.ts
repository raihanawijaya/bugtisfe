import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownModule } from 'primeng/dropdown';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { PicCreateComponent } from './pic-create/pic-create.component';
import { PicUpdateComponent } from './pic-update/pic-update.component';
import { PicListComponent } from './pic-list/pic-list.component';
import { PicRoutingModule } from './pic-routing.module';



@NgModule({
  declarations: [
    PicCreateComponent,
    PicUpdateComponent,
    PicListComponent
  ],
  imports: [
    CommonModule,
    PicRoutingModule,
    FormsModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    RadioButtonModule,
    ConfirmDialogModule,
    DropdownModule
  ],
  exports: [
    PicCreateComponent,
    PicUpdateComponent,
    PicListComponent
  ]
})
export class PicModule { }
