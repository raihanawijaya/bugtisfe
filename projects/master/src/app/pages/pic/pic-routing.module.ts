import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PicCreateComponent } from './pic-create/pic-create.component';
import { PicListComponent } from './pic-list/pic-list.component';
import { PicUpdateComponent } from './pic-update/pic-update.component';

const routes : Routes = [
  {
    path : "new/:id",
    component: PicCreateComponent,
  },
  {
    path : ":id",
    component: PicListComponent,
  },
  {
    path : "edit/:id",
    component: PicUpdateComponent,
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PicRoutingModule { }
