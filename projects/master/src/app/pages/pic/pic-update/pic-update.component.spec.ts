import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicUpdateComponent } from './pic-update.component';

describe('PicUpdateComponent', () => {
  let component: PicUpdateComponent;
  let fixture: ComponentFixture<PicUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PicUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PicUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
