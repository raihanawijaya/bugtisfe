import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'projects/ticketing/src/app/services/auth.service';
import { UpdatePicReq } from '../../../models/pic/update-pic-req';
import { FindAllProductResponseData } from '../../../models/products/find-all-product-res-data';
import { PicService } from '../../../services/pic.service';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-pic-update',
  templateUrl: './pic-update.component.html',
  styleUrls: ['./pic-update.component.css']
})
export class PicUpdateComponent implements OnInit {

  product: FindAllProductResponseData[] = []

  pic = new UpdatePicReq()

  constructor(private title: Title, private route: Router, private picService: PicService,
    private activatedRoute: ActivatedRoute, private productService: ProductService, 
    private location: Location, private authService: AuthService) { }

  ngOnInit(): void {
    this.title.setTitle('Edit Account List')

    if (this.authService.getProfile().roleCode == 'ADM') {
      this.productService.findAll().subscribe(respon => {
        this.product = respon.data!
      })
    } else {
      this.productService.findByIdAccount().subscribe(respon => {
        this.product = respon.data!
      })
    }

    
 
      this.activatedRoute.params.subscribe(res => {
        this.picService.findById(res.id).subscribe(res => {
          this.pic.id = res.data?.id
          this.pic.account = res.data?.account
          this.pic.product = res.data?.product
          this.pic.version = res.data?.version
          this.pic.isActive = res.data?.isActive
        })
      })


    this.activatedRoute.params.subscribe(res => {
      this.pic.account = res.id
    })

  }

  onClick(): void {
    this.picService.update(this.pic).subscribe(res => {
      this.back()
    })
  }

  back(): void {
    this.location.back()
  }

}
