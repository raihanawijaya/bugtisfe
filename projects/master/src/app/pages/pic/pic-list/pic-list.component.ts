import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { FindByIdAccountResData } from '../../../models/accounts/find-by-id-account-res-data';
import { FindAllPicResData } from '../../../models/pic/find-all-pic-res-data';
import { AccountService } from '../../../services/account.service';
import { PicService } from '../../../services/pic.service';

@Component({
  selector: 'app-pic-list',
  templateUrl: './pic-list.component.html',
  styleUrls: ['./pic-list.component.css'],
  providers: [ConfirmationService]
})
export class PicListComponent implements OnInit {

  constructor(private accountlistService: PicService, private route: Router, private title: Title,
    private confirmationService: ConfirmationService, private activatedRoute: ActivatedRoute,
    private accountService: AccountService) { }

  data: FindAllPicResData[] = []
  dataAccount: FindByIdAccountResData = new FindByIdAccountResData()
  id = ""

  confirm(id?: string) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.accountlistService.delete(id!).subscribe(res => {
          this.initData()
        })
      },
      reject: () => {
        this.initData()
      }
    });
  }

  ngOnInit(): void {
    this.title.setTitle('Account List')

    this.activatedRoute.params.subscribe(res => {
      this.id = res.id
      this.initData()
      this.account()
    })
  }

  initData(): void {
    this.accountlistService.findByAccount(this.id).toPromise().then(res => {
      this.data = res.data!
    })
  }

  account(): void {
    this.activatedRoute.params.subscribe(res => {
      this.accountService.findById(res.id).subscribe(res => {
        this.dataAccount = res.data!
        console.log(this.dataAccount);
      })
    })
  }

  update(id?: number): void {
    this.route.navigate(['/pic/edit/' + id])
  }

  onClick(): void {
    this.route.navigate(['/pic/new/' + this.id])
  }
}

