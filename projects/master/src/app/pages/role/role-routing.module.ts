import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoleCreateComponent } from './role-create/role-create.component';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleUpdateComponent } from './role-update/role-update.component';
import { CanAccessService } from 'projects/ticketing/src/app/services/can-access.service';
import { CanAuthorizeService } from 'projects/ticketing/src/app/services/can-authorize.service';

const routes: Routes = [
  {
    path: "",
    component: RoleListComponent,
    
  },
  {
    path: "new",
    component: RoleCreateComponent,
  },
  {
    path: "edit/:id",
    component: RoleUpdateComponent,
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RoleRoutingModule { }
