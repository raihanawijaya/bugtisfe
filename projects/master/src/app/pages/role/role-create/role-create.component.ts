import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { InsertRoleRequest } from '../../../models/roles/insert-role-req';
import { RoleService } from '../../../services/role.service';

@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.css']
})
export class RoleCreateComponent implements OnInit {

  role = new InsertRoleRequest()

  constructor(private title: Title, private roleService : RoleService, private route : Router) { }

  ngOnInit(): void {
    this.title.setTitle('Add Role')
  }

  onClick(): void {
    this.roleService.insert(this.role).subscribe(res => {
      this.route.navigate(['roles'])
    })
  }

}
