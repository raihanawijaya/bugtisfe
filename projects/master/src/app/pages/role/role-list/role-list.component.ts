import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { FindAllRoleResponseData } from '../../../models/roles/find-all-role-res-data';
import { RoleService } from '../../../services/role.service';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css'],
  providers: [ConfirmationService]
})
export class RoleListComponent implements OnInit {

  constructor(private roleService: RoleService, private route: Router,
    private title: Title, private confirmationService: ConfirmationService) { }

  data: FindAllRoleResponseData[] = []

  confirm(id?: string) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.roleService.delete(id!).subscribe(res => {
          this.initData()
        })
      },
      reject: () => {
        this.initData()
      }
    });
  }

  ngOnInit(): void {
    this.initData()
    this.title.setTitle('Roles')
  }

  initData(): void {
    this.roleService.findAll().toPromise().then(res => {
      this.data = res.data!
    })
  }

  update(id?: number): void {
    this.route.navigate(['/roles/edit/' + id])
  }

}
