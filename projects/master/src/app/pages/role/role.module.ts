import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleCreateComponent } from './role-create/role-create.component';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleUpdateComponent } from './role-update/role-update.component';
import { RoleRoutingModule } from './role-routing.module';

import {TableModule} from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import {RadioButtonModule} from 'primeng/radiobutton';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

@NgModule({
  declarations: [
    RoleCreateComponent,
    RoleListComponent,
    RoleUpdateComponent,
  ],
  imports: [
    CommonModule,
    RoleRoutingModule,
    FormsModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    RadioButtonModule,
    ConfirmDialogModule,
  ],
  exports : [
    RoleCreateComponent,
    RoleListComponent,
    RoleUpdateComponent
  ]
})
export class RoleModule { }
