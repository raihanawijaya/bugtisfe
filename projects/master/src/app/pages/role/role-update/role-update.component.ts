import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { UpdateRoleRequest } from '../../../models/roles/update-role-req';
import { RoleService } from '../../../services/role.service';

@Component({
  selector: 'app-role-update',
  templateUrl: './role-update.component.html',
  styleUrls: ['./role-update.component.css']
})
export class RoleUpdateComponent implements OnInit {

  role = new UpdateRoleRequest()

  constructor(private title: Title, private roleService: RoleService,
    private route: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.title.setTitle('Edit Role')

    this.activatedRoute.params.subscribe(res => {
      this.roleService.findById(res.id).subscribe(res => {
        this.role.id = res.data?.id
        this.role.code = res.data?.code
        this.role.name = res.data?.name
        this.role.version = res.data?.version
        this.role.isActive = res.data?.isActive
      })
    })
  }

  onClick() : void {
    this.roleService.update(this.role).subscribe(res => {
      this.route.navigate(['roles'])
    })
  }

}
