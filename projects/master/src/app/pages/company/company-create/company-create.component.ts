import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { InsertCompanyRequest } from '../../../models/companies/insert-company-req';
import { CompanyService } from '../../../services/company.service';

@Component({
  selector: 'app-company-create',
  templateUrl: './company-create.component.html',
  styleUrls: ['./company-create.component.css']
})
export class CompanyCreateComponent implements OnInit {

  company = new InsertCompanyRequest()

  constructor(private title: Title, private companyService : CompanyService, private route : Router) { }

  ngOnInit(): void {
    this.title.setTitle('Add Company')
  }

  onClick(): void {
    this.companyService.insert(this.company).subscribe(res => {
      this.route.navigate(['companies'])
    })
  }

}
