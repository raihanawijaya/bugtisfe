import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { CompanyCreateComponent } from './company-create/company-create.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyRoutingModule } from './company-routing.module';
import { CompanyUpdateComponent } from './company-update/company-update.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';



@NgModule({
  declarations: [
    CompanyListComponent,
    CompanyUpdateComponent,
    CompanyCreateComponent
  ],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    FormsModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    RadioButtonModule,
    ConfirmDialogModule,
  ],
  exports: [
    CompanyListComponent,
    CompanyUpdateComponent,
    CompanyCreateComponent
  ]
})
export class CompanyModule { }
