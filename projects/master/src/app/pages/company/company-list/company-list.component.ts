import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { FindAllCompanyResponseData } from '../../../models/companies/find-all-company-res-data';
import { CompanyService } from '../../../services/company.service';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css'],
  providers: [ConfirmationService]
})
export class CompanyListComponent implements OnInit {

  constructor(private companyService: CompanyService, private route: Router,
    private title: Title, private confirmationService: ConfirmationService) { }

  data: FindAllCompanyResponseData[] = []

  confirm(id?: number) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.companyService.delete(id!).subscribe(res => {
          this.initData()
        })
      },
      reject: () => {
        this.initData()
      }
    });
  }

  ngOnInit(): void {
    this.initData()
    this.title.setTitle('Company')
  }

  initData(): void {
    this.companyService.findAll().toPromise().then(res => {
      this.data = res.data!
    })
  }

  update(id?: number): void {
    this.route.navigate(['/companies/edit/' + id])
  }

}
