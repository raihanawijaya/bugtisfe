import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { UpdateCompanyRequest } from '../../../models/companies/update-company-req';
import { CompanyService } from '../../../services/company.service';

@Component({
  selector: 'app-company-update',
  templateUrl: './company-update.component.html',
  styleUrls: ['./company-update.component.css']
})
export class CompanyUpdateComponent implements OnInit {

  company = new UpdateCompanyRequest()

  constructor(private title: Title, private companyService: CompanyService,
    private route: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.title.setTitle('Edit Compant')

    this.activatedRoute.params.subscribe(res => {
      this.companyService.findById(res.id).subscribe(res => {
        this.company.id = res.data?.id
        this.company.code = res.data?.code
        this.company.name = res.data?.name
        this.company.address = res.data?.address
        this.company.phone = res.data?.phone
        this.company.version = res.data?.version
        this.company.isActive = res.data?.isActive
      })
    })
  }

  onClick(): void {
    this.companyService.update(this.company).subscribe(res => {
      this.route.navigate(['companies'])
    })
  }

}
