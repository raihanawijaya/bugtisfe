import { FindByIdStatusResponseData } from "./find-by-id-status-res-data"

export class FindByIdStatusResponse {
    msg?: string
    data?: FindByIdStatusResponseData
}