import { FindAllStatusResponseData } from "./find-all-status-res-data"

export class FindAllStatusResponse {
    msg?: string
    data?: FindAllStatusResponseData[]
}