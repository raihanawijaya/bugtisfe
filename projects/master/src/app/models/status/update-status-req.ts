export class UpdateStatusRequest {
    id?: string
    code?: string
    name?: string
    version?: number
    isActive?: boolean
}