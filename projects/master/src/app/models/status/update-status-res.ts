import { UpdateStatusResponseData } from "./update-status-res-data"

export class UpdateStatusResponse {
    msg?: string
    data?: UpdateStatusResponseData
}