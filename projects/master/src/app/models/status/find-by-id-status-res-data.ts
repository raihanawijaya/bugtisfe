export class FindByIdStatusResponseData {
    id?: string
    name?: string
    code?: string
    version?: number
    isActive?: boolean
}