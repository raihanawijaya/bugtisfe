import { InsertStatusResponseData } from "./insert-status-res-data"

export class InsertStatusResponse {
    mgs?: string
    data?: InsertStatusResponseData
}