export class FindAllStatusResponseData {
    id?: string
    code?: string
    nama?: string
    isActive?: boolean
}