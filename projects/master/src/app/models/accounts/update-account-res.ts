import { UpdateAccountResData } from "./update-account-res-data"

export class UpdateAccountRes {
    msg?: string
    data?: UpdateAccountResData
}