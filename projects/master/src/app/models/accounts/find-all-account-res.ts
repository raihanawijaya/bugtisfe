import { FindAllAccountResData } from "./find-all-account-res-data"

export class FindAllAccountRes {
    msg?: string
    data?: FindAllAccountResData[]
}