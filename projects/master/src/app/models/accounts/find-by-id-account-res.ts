import { FindByIdAccountResData } from "./find-by-id-account-res-data"

export class FindByIdAccountRes {
    msg?: string
    data?: FindByIdAccountResData
}