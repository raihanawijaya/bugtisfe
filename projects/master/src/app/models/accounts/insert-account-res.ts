import { InsertAccountResData } from "./insert-account-res-data"

export class InsertAccountRes {
    msg?: string
    data?: InsertAccountResData
}