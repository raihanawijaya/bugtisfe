export class FindByIdAccountResData {
    id?: string
    company?: string
    user?: string
    version?: number
    isActive?: boolean
}