export class UpdateAccountReq {
    id?: string
    company?: string
    user?: string
    version?: number
    isActive?: boolean
}