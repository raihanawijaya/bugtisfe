import { FindAllProductByIdAccountResponseData } from "./find-all-product-by-id-account-res-data"

export class FindAllProductByIdAccountResponse {
    msg?: string
    data?: FindAllProductByIdAccountResponseData[]
}