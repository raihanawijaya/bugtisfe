import { FindAllProductResponseData } from "./find-all-product-res-data"

export class FindAllProductResponse {
    msg?: string
    data?: FindAllProductResponseData[]
}