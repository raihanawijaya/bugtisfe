import { FindByIdProductResponseData } from "./find-by-id-product-res-data"

export class FindByIdProductResponse {
    msg?: string
    data?: FindByIdProductResponseData
}