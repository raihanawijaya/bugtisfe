export class FindAllProductResponseData {
    id?: string
    code?: string
    name?: string
}