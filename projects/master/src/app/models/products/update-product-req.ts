export class UpdateProductRequest {
    id?: string
    code?: string
    name?: string
    version?: number
    isActive?: boolean
}