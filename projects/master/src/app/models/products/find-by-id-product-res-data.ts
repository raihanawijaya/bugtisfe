export class FindByIdProductResponseData {
    id?: string
    name?: string
    code?: string
    version?: number
    isActive?: boolean
}