import { UpdateProductResponseData } from "./update-product-res-data"

export class UpdateProductResponse {
    msg?: string
    data?: UpdateProductResponseData
}