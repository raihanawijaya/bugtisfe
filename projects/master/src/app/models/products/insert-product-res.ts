import { InsertProductResponseData } from "./insert-product-res-data"

export class InsertProductResponse {
    mgs?: string
    data?: InsertProductResponseData
}