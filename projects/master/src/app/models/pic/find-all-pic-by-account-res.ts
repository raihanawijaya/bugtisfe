import { FindAllPicByAccountResData } from "./find-all-pic-by-account-res-data"

export class FindAllPicByAccountRes {
    msg?: string
    data?: FindAllPicByAccountResData[]
}