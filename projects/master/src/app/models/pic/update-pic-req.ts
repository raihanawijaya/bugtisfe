export class UpdatePicReq {
    id?: string
    account?: string
    product?: string
    version?: number
    isActive?: boolean
}