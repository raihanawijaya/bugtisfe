export class FindByIdPicResData {
    id?: string
    account?: string
    product?: string
    version?: number
    isActive?: boolean
}