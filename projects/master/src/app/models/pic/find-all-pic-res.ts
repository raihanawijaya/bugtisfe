import { FindAllPicResData } from "./find-all-pic-res-data"

export class FindAllPicRes {
    msg?: string
    data?: FindAllPicResData[]
}