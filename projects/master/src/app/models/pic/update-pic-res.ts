import { UpdatePicResData } from "./update-pic-res-data"

export class UpdatePicRes {
    msg?: string
    data?: UpdatePicResData
}