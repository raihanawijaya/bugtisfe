import { FindByIdPicResData } from "./find-by-id-pci-res-data"

export class FindByIdPicRes {
    msg?: string
    data?: FindByIdPicResData
}