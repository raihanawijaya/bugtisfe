import { InsertPicResData } from "./insert-pic-res-data"

export class InsertPicRes {
    msg?: string
    data?: InsertPicResData
}