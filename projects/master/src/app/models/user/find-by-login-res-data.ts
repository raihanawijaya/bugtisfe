import { FindAllProductUsersResData } from "./find-all-product-users-res-data"

export class FindByLoginResData {
    id?: string
    name?: string
    email?: string
    pass?: string
    company?: string
    product?: FindAllProductUsersResData[]
}