import { FindAllProductUsersResData } from "./find-all-product-users-res-data"

export class FindAllProductUsersRes{
    msg?: string
    data?: FindAllProductUsersResData[]
}