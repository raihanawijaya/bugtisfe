import { FindAllUserResData } from "./find-all-user-res-data"

export class FindAllUserRes {
    msg? : string
    data? : [FindAllUserResData]
}