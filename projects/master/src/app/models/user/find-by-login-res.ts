import { FindByLoginResData } from "./find-by-login-res-data"

export class FindByLoginRes {
    msg?: string
    data?: FindByLoginResData
}