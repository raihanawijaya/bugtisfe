import { FindUserByIdResData } from "./find-user-by-id-res-data"

export class FindUserByIdRes {
    msg? : string
    data? : FindUserByIdResData
}