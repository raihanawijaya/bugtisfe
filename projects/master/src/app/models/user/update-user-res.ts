import { UpdateUserResData } from "./update-user-res-data"

export class UpdateUserRes {
    msg? : string
    data? : [UpdateUserResData]
}