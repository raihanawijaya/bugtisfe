export class InsertUserReq {
    name? : string
    email? : string
    pass? : string
    role? : string
    isActive?: boolean
}