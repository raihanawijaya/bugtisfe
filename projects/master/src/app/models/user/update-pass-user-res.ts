import { UpdatePassUsersResData } from "./update-pass-user-res-data"

export class UpdatePassUsersRes {
    msg?: string
    data?: UpdatePassUsersResData
    }