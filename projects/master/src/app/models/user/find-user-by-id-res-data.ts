export class FindUserByIdResData {
    id? : string
    name? : string
    email? : string
    role? : string
    image? : string
    pass? : string
    version? : number
    isActive? : boolean
}