export class FindAllUserResData {
    id? : string
    name? : string
    email? : string
    role? : string
    image? : string
}