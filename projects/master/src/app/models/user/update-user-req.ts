export class UpdateUserReq {
    id? : string
    version? : number
    name? : string
    email? : string
    pass? : string
    image? : string
    role? : string
    isActive? : boolean
}