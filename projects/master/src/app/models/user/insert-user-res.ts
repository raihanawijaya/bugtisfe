import { InsertUserResData } from "./insert-user-res-data"

export class InsertUserRes {
    msg? : string
    data? : [InsertUserResData]
}