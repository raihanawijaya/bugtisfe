export class FindByIdCompanyResponseData {
    id?: string
    code?: string
    name?: string
    address?: string
    phone?: string
    version?: number
    isActive?: boolean
}