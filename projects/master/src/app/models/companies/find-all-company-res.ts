import { FindAllCompanyResponseData } from "./find-all-company-res-data"

export class FindAllCompanyResponse {
    msg?: string
    data?: FindAllCompanyResponseData[]
}