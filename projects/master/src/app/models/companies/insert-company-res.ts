import { InsertCompanyResponseData } from "./insert-company-res-data"

export class InsertCompanyResponse {
    msg?: string
    data?: InsertCompanyResponseData
}