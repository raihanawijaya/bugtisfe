import { FindByIdCompanyResponseData } from "./find-by-id-company-res-data"

export class FindByIdCompanyResponse{
    msg?: string
    data?: FindByIdCompanyResponseData
}