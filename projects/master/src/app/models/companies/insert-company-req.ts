export class InsertCompanyRequest {
    code?: string
    name?: string
    address?: string
    phone?: string
}