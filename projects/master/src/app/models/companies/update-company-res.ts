import { UpdateRoleResponseData } from "../roles/update-role-res-data"

export class UpdateCompanyResponse {
    msg?: string
    data?: UpdateRoleResponseData
}