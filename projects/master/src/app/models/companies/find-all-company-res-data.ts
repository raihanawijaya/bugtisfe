export class FindAllCompanyResponseData {
    id?: string
    code?: string
    name?: string
    address?: string
    phone?: string
}