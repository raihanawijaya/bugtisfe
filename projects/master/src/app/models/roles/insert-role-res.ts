import { InsertRoleResponseData } from "./insert-role-res-data"

export class InsertRoleResponse {
    mgs?: string
    data?: InsertRoleResponseData
}