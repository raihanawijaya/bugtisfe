import { FindAllRoleResponseData } from "./find-all-role-res-data"

export class FindAllRoleResponse {
    msg?: string
    data?: FindAllRoleResponseData[]
}