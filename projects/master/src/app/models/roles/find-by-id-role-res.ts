import { FindByIdRoleResponseData } from "./find-by-id-role-res-data"

export class FindByIdRoleResponse {
    msg?: string
    data?: FindByIdRoleResponseData
}