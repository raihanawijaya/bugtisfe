export class UpdateRoleRequest {
    id?: string
    code?: string
    name?: string
    version?: number
    isActive?: boolean
}