import { UpdateRoleResponseData } from "./update-role-res-data"

export class UpdateRoleResponse {
    msg?: string
    data?: UpdateRoleResponseData
}