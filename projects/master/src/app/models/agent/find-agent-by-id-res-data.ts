export class FindAgentByIdResData {
    id? : string
    user? : string
    product? : string
    version? : number
    isActive? : boolean
}