import { InsertAgentResData } from "./insert-agent-res-data"

export class InsertAgentRes {
    msg? : string
    data? : [InsertAgentResData]
}