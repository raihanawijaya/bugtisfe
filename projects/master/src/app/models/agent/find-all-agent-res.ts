import { FindAllAgentResData } from "./find-all-agent-res-data"

export class FindAllAgentRes {
    msg? : string
    data? : [FindAllAgentResData]
}