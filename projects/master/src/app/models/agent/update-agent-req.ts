export class UpdateAgentReq {
    id? : string
    user? : string
    product? : string
    version? : number
    isActive? : boolean
}