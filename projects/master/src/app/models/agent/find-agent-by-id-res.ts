import { FindAgentByIdResData } from "./find-agent-by-id-res-data"

export class FindAgentByIdRes {
    msg? : string
    data? : FindAgentByIdResData
}