import { UpdateAgentResData } from "./update-agent-res-data"

export class UpdateAgentRes {
    msg? : string
    data? : [UpdateAgentResData]
}