import { CountTicketResData } from "./count-ticket-res-data"

export class CountTicketRes {
    msg?: string
    data?: CountTicketResData
}