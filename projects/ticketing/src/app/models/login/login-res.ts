import { LoginResData } from "./login-res-data"

export class LoginRes {
    msg? : string
    data? : LoginResData
}