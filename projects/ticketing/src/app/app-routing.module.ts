import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './pages/menu/menu.component';
import { CanAccessService } from './services/can-access.service';
import { CanAuthorizeService } from './services/can-authorize.service';

const routes: Routes = [
  {
    path: "",
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: "",
    loadChildren: () => import('./pages/login/login.module').then(r => r.LoginModule)
  },
  {
    path: "",
    loadChildren: () => import('./pages/menu/menu.module').then(r => r.MenuModule)
  },
  {
    path: "",
    component: MenuComponent,
    loadChildren: () => import('./pages/home/home.module').then(r => r.HomeModule)
  },
  {
    path: "",
    component: MenuComponent,
    loadChildren: () => import('./pages/profile/profile.module').then(r => r.ProfileModule)
  },
  {
    path: "roles",
    component: MenuComponent,
    loadChildren: () => import('../../../master/src/app/pages/role/role.module').then(r => r.RoleModule),
    canActivate: [
      CanAccessService,
      CanAuthorizeService
    ]
  },
  {
    path: "status",
    component: MenuComponent,
    loadChildren: () => import('../../../master/src/app/pages/status/status.module').then(r => r.StatusModule),
    canActivate: [
      CanAccessService,
      CanAuthorizeService
    ]
  },
  {
    path: "companies",
    component: MenuComponent,
    loadChildren: () => import('../../../master/src/app/pages/company/company.module').then(r => r.CompanyModule),
    canActivate: [
      CanAccessService,
      CanAuthorizeService
    ]
  },
  {
    path: "products",
    component: MenuComponent,
    loadChildren: () => import('../../../master/src/app/pages/product/product.module').then(r => r.ProductModule),
    canActivate: [
      CanAccessService,
      CanAuthorizeService
    ]
  },
  {
    path: "accounts",
    component: MenuComponent,
    loadChildren: () => import('../../../master/src/app/pages/account/account.module').then(r => r.AccountModule),
    canActivate: [
      CanAccessService,
      CanAuthorizeService
    ]
  },
  {
    path: "pic",
    component: MenuComponent,
    loadChildren: () => import('../../../master/src/app/pages/pic/pic.module').then(r => r.PicModule),
    canActivate: [
      CanAccessService,
      CanAuthorizeService
    ]
  },
  {
    path: "users",
    component: MenuComponent,
    loadChildren: () => import('../../../master/src/app/pages/user/user.module').then(r => r.UserModule),
    canActivate: [
      CanAccessService,
      CanAuthorizeService
    ]
  },
  {
    path: "agents",
    component: MenuComponent,
    loadChildren: () => import('../../../master/src/app/pages/agent/agent.module').then(r => r.AgentModule),
    canActivate: [
      CanAccessService,
      CanAuthorizeService
    ]
  },
  {
    path: "tickets",
    component: MenuComponent,
    loadChildren: () => import('../../../transaction/src/app/pages/thread-header/thread-header.module').then(r => r.ThreadHeaderModule),
    
  },
  {
    path: "tickets/dtl",
    component: MenuComponent,
    loadChildren: () => import('../../../transaction/src/app/pages/thread-detail/thread-detail.module').then(r => r.ThreadDetailModule),
   
  },
  
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
