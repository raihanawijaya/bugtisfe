import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu.component';
import { RouterModule, Routes } from '@angular/router';
import { CanAccessService } from '../../services/can-access.service';
import { CanAuthorizeService } from '../../services/can-authorize.service';

const routes: Routes = [
  {
    path: "menu",
    component: MenuComponent,
    canActivate: [
      CanAccessService
    ]
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class MenuRoutingModule { }
