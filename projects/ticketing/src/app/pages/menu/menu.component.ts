import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { FindUserByIdResData } from 'projects/master/src/app/models/user/find-user-by-id-res-data';
import { UserService } from 'projects/master/src/app/services/user.service';
import { LoginResData } from '../../models/login/login-res-data';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  isAdmin = false
  isAgent = false
  isCustomer = false
  isAccount = false
  isHome = false
  isMaster = false
  isProfile = false
  isTicket = false
  isRole = false
  isUser = false
  isStatus = false
  isProduct = false
  isCompany = false
  isAgentList = false
  isAccountList = false
  visibleTopBar?: boolean

  itemsAdmin: MenuItem[] = []
  activeItem?: MenuItem
  dataUser: FindUserByIdResData = new FindUserByIdResData()
  dataLogin: LoginResData = new LoginResData()
  idLogin: string = ""
  idDecode: any

  constructor(private authService: AuthService, private route: Router,
    private activatedRoute: ActivatedRoute, private userService: UserService) { }

  ngOnInit(): void {

    this.activeItem = this.itemsAdmin[0];
    if (this.authService.getProfile().roleCode == 'ADM') {
      this.isAdmin = true
    } else if (this.authService.getProfile().roleCode == 'AGN') {
      this.isAgent = true
    } else if (this.authService.getProfile().roleCode == 'CST') {
      this.isCustomer = true
    } else if (this.authService.getProfile().roleCode == 'ACC') {
      this.isAccount = true
    }


    if (this.route.url == "/menu" || this.isAdmin) {
      this.visibleTopBar = true
    }

    if (this.route.url == '/home') {
      this.isHome = true
      this.visibleTopBar = false
    } else if (this.route.url == "/roles" || this.route.url == "/users" || this.route.url == "/accounts" || this.route.url.includes("/pic/")) {
      this.isMaster = true
      this.visibleTopBar = true
    } else if (this.route.url == "/tickets" || this.route.url.includes("/thread/dtl/create/")) {
      this.isTicket = true
      this.visibleTopBar = false
    } else if (this.route.url.includes("/profile")) {
      this.isProfile = true
      this.visibleTopBar = false
    }

    if (this.route.url == "/roles" || this.route.url == "/roles/new" || this.route.url == "/roles/edit") {
      this.isRole = true
      this.isMaster = true
    } else if (this.route.url == "/users" || this.route.url == "/users/new" || this.route.url == "/users/edit") {
      this.isUser = true
      this.isMaster = true
    } else if (this.route.url == "/status" || this.route.url == "/status/new" || this.route.url == "/status/edit") {
      this.isStatus = true
      this.isMaster = true
    } else if (this.route.url == "/products" || this.route.url == "/products/new" || this.route.url == "/products/edit") {
      this.isProduct = true
      this.isMaster = true
    } else if (this.route.url == "/companies" || this.route.url == "/companies/new" || this.route.url == "/companies/edit") {
      this.isCompany = true
      this.isMaster = true
    } else if (this.route.url == "/agents" || this.route.url == "/agents/new" || this.route.url == "/agents/edit") {
      this.isAgentList = true
      this.isMaster = true
    } else if (this.route.url == "/accounts" || this.route.url == "/accounts/new" || this.route.url == "/accounts/edit" ||
    this.route.url.includes("/pic/")) {
      this.isAccountList = true
      this.isMaster = true
    }
  }

  logout(): void {
    this.authService.onLogout()
    this.route.navigate(['/login'])
  }

  findIdUser(id?: number): void {
    this.activatedRoute.params.subscribe(res => {
      this.userService.findById(res.id).subscribe(res => {
        this.dataUser = res.data!
      })
    })
  }
}
