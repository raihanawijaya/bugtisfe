import { Component, OnInit } from '@angular/core';
import { CountTicketResData } from '../../models/count-ticket/count-ticket-res-data';
import { AuthService } from '../../services/auth.service';
import { CountTicketService } from '../../services/count-ticket.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private countTicketService: CountTicketService, private authService : AuthService) { }

  data: CountTicketResData = {
    total : 0,
    totalOpen : 0,
    totalClose : 0,
    totalReopen : 0
  }

  ngOnInit(): void {

    this.countTicketService.countTicket().toPromise().then(res => {
      if(res.data){
        this.data = res.data!
      } 
    })
  }

}
