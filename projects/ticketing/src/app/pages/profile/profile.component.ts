import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { FindAllRoleResponseData } from 'projects/master/src/app/models/roles/find-all-role-res-data';
import { FindAllProductUsersResData } from 'projects/master/src/app/models/user/find-all-product-users-res-data';
import { FindByLoginResData } from 'projects/master/src/app/models/user/find-by-login-res-data';
import { UpdatePassUsersReq } from 'projects/master/src/app/models/user/update-pass-user-req';
import { RoleService } from 'projects/master/src/app/services/role.service';
import { UserService } from 'projects/master/src/app/services/user.service';
import { LoginResData } from '../../models/login/login-res-data';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ConfirmationService]
})
export class ProfileComponent implements OnInit {

  data = new UpdatePassUsersReq();
  dataUser: FindByLoginResData = new FindByLoginResData()
  dataRoles: FindAllRoleResponseData[] = []
  dataProductUser: FindAllProductUsersResData[] = []
  dataLogin: LoginResData = new LoginResData()
  status = ""
  idLogin: any
  isAgent = false

  constructor(private route: Router, private userService: UserService, private rolesService: RoleService,
    private authService: AuthService, private activatedRoute: ActivatedRoute, private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.authService.getProfile().token
    this.findIdUser()
    if (this.authService.getProfile().roleCode == 'AGN') {
      this.isAgent = true
    } else {
      this.isAgent = false
    }
  }

  findIdUser(id?: number): void {
    this.idLogin = this.authService.getProfile().id
    this.userService.findByLogin(this.idLogin).subscribe(res => {
      this.userService.findById(this.idLogin).subscribe(res => {
        this.data.id = this.idLogin
        this.data.pass = res.data?.pass
        this.data.version = res.data?.version
      })
      this.dataUser = res.data!
      this.dataProductUser = res.data?.product!
    })

  }

  confirm() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to change password?',
      accept: () => {
        this.onClick();
      },
      reject: () => {
      }
    });
  }

  onClick(): void {
    this.userService.updatePass(this.data).subscribe(res => {
      this.route.navigate(['/profile'])
    })
  }
}
