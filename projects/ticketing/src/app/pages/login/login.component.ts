import { Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { LoginReqData } from "../../models/login/login-req-data";
import { AuthService } from "../../services/auth.service";
import { UserService } from "../../services/user.service";



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  dataLogin = new LoginReqData();

  constructor(private route: Router, private userService: UserService, private title : Title, private authService: AuthService) { }
  
  ngOnInit(): void {
    this.title.setTitle('Bugtis')
  }

  login(): void {
    this.userService.login(this.dataLogin).subscribe(res=>{
      this.authService.saveProfile(res.data!)
      this.route.navigate(['/home'])
    })
  }

}
