import { TestBed } from '@angular/core/testing';

import { CountTicketService } from './count-ticket.service';

describe('CountTicketService', () => {
  let service: CountTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CountTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
