import { Injectable } from '@angular/core';
import { LoginResData } from '../models/login/login-res-data';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  saveProfile(profile: LoginResData){
    localStorage.setItem('profile',JSON.stringify(profile))
  }

  isLogin(): boolean {
    let profile = localStorage.getItem('profile')
    return profile != null && profile != undefined;
  }

  onLogout(): void {
    localStorage.clear();
  }

  getProfile(): LoginResData {
    let profile = localStorage.getItem('profile')
    if (profile) {
      return JSON.parse(profile)
    }
    return new LoginResData()
  }
}
