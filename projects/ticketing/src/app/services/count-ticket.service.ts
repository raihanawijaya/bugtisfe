import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'projects/master/src/app/constant/general-constant';
import { Observable } from 'rxjs';
import { CountTicketRes } from '../models/count-ticket/count-ticket-res';

@Injectable({
  providedIn: 'root'
})
export class CountTicketService {

  constructor(private http: HttpClient) { }

  countTicket(): Observable<CountTicketRes> {
    return this.http.get(`${BASE_URL}/thread-hdr/count`)
  }

}
