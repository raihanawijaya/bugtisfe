import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FindAllRoleResponse } from 'projects/master/src/app/models/roles/find-all-role-res';
import { Observable } from 'rxjs';
import { LoginReqData } from '../models/login/login-req-data';
import { LoginRes } from '../models/login/login-res';
import { BASE_URL } from '../../../../master/src/app/constant/general-constant';
import { FindUserByIdResData } from 'projects/master/src/app/models/user/find-user-by-id-res-data';
import { FindUserByIdRes } from 'projects/master/src/app/models/user/find-user-by-id-res';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<FindAllRoleResponse> {
    return this.http.get(`${BASE_URL}/roles`)
  }

  findById(id: number): Observable<FindUserByIdRes> {
    return this.http.get(`${BASE_URL}/users/${id}`)
  }

  login(body : LoginReqData) : Observable<LoginRes> {
    return this.http.post(`${BASE_URL}/api/login`,body)
  }

}

