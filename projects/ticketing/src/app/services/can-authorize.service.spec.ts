import { TestBed } from '@angular/core/testing';

import { CanAuthorizeService } from './can-authorize.service';

describe('CanAuthorizeService', () => {
  let service: CanAuthorizeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CanAuthorizeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
