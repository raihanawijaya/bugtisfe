import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CanAuthorizeService {

  adminPath = ['roles', 'status', 'users', 'products', 'companies', 'agents', 'accounts', 'pic',]
  customerPath = ['users', 'accounts', 'pic', 'profile']

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const path = route.url[0].path

    if (this.authService.getProfile().roleCode == 'ADM') {
      for (let i = 0; i < this.adminPath.length; i++) {
        if (path.includes(this.adminPath[i])) {
          return true;
        }
      }
    }else if (this.authService.getProfile().roleCode == 'CST'){
      for (let i = 0; i < this.customerPath.length; i++) {
        if (path.includes(this.customerPath[i])) {
          return true;
        }
      }
    } else {
      this.router.navigate(['home'])
      return false;
    }

    if (path != 'menu') {
      this.router.navigate(['menu'])
    }
    this.router.navigate(['home'])
    return false;
  }
}
